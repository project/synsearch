<?php

namespace Drupal\synsearch\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SearchForm.
 */
class SearchForm extends FormBase {

  const ROUTE = "synsearch.search_controller_page";

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'syn_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $small = NULL) {
    $title = !empty($_GET['title']) ? $_GET['title'] : '';
    $form['#attributes']['class'] = [
      'synsearch-form',
      'contact-message-form',
      'contact-message-form--label',
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter a search keyword'),
      '#maxlength' => 64,
      '#size' => 64,
      '#attributes' => [
        'placeholder' => $this->t('product name'),
      ],
      '#weight' => '0',
      '#default_value' => $title,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['btn--submit'],
      ],
    ];
    unset($form['title']['#title']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $route = self::ROUTE;
    $query = [];
    $exclude = $form_state->getCleanValueKeys();
    foreach ($form_state->getValues() as $key => $value) {
      if (!in_array($key, $exclude) && !is_object($value)) {
        $query[$key] = $value;
      }
    }
    $form_state->setRedirect(
      $route,
      [],
      ['query' => $query]
    );
  }

}
