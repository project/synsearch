<?php

namespace Drupal\synsearch\Hook;

/**
 * Implements preprocess page hook.
 */
class PreprocessPage {

  /**
   * Run hook.
   */
  public static function hook(&$variables) {
    $variables['searchForm'] = \Drupal::formBuilder()
      ->getForm('\Drupal\synsearch\Form\SearchForm', TRUE);
  }

}
