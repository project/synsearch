<?php

namespace Drupal\synsearch\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;

// use Drupal\commerce_product\Entity\Product;
// use Drupal\node\Entity\Node;
/**
 * Class SearchController.
 */
class SearchController extends ControllerBase {

  const ITEMS_LIMIT = 8;
  const SORT = FALSE;

  /**
   * Construct new controlelr instance.
   */
  public function __construct() {
    $this->index_storage = \Drupal::entityTypeManager()
      ->getStorage('search_api_index');
    $getData = \Drupal::request()->query;

    $this->title = (string) $getData->get('title');
    $this->title = mb_strtolower($this->title);
    $this->page = (int) $getData->get('page', 0);
  }

  /**
   * Search page.
   */
  public function page() {
    return [
      '#theme' => 'synapse-search-page',
      '#data' => [
        'form' => \Drupal::formBuilder()->getForm('\Drupal\synsearch\Form\SearchForm'),
        'title' => $this->title,
      ],
    ];
  }

  /**
   * Get search result.
   */
  public function getList() {
    $this->makeBaseResponseArray();
    if (!empty($this->title)) {
      $this->searchByProducts();
      $this->searchByNodes();

      if (self::SORT && !empty($this->response['list'])) {
        $this->associativeArraySortWithKey($this->response['list'], 'changed', TRUE);
      }
    }
    return new JsonResponse($this->response);
  }

  /**
   * Search in commerce products table by title.
   */
  private function searchByProducts() {
    if (!empty($products = $this->getProducts())) {
      $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('commerce_product');
      foreach ($products as $product) {
        $view = $viewBuilder->view($product, 'teaser');
        $this->response['list'][] = [
          'html' => \Drupal::service('renderer')->renderRoot($view),
          'changed' => $product->getChangedTime(),
        ];
      }
    }
  }

  /**
   * Get products.
   */
  private function getProducts() {
    $result = $this->entitiesBaseQuery('commerce_product');
    $productsTotal = $result->getResultCount();
    $this->response['total'] += $productsTotal;

    $watchedNow = self::ITEMS_LIMIT * ($this->page + 1);
    if ($watchedNow > $productsTotal) {
      $watchedNow = $productsTotal;
    }
    $this->response['watched'] += $watchedNow;

    $products = [];
    $count = 0;
    foreach ($result->getResultItems() as $item) {
      if ($count >= ($this->page * self::ITEMS_LIMIT) && count($products) < self::ITEMS_LIMIT) {
        $products[] = $item->getOriginalObject()->getEntity();
      }
      $count++;
    }
    return $products;
  }

  /**
   * Search in nodes table by title.
   */
  private function searchByNodes() {
    if (!empty($nodes = $this->getNodes())) {
      $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('node');
      foreach ($nodes as $node) {
        $view = $viewBuilder->view($node, 'teaser');
        $this->response['list'][] = [
          'html' => \Drupal::service('renderer')->renderRoot($view),
          'changed' => $node->getChangedTime(),
        ];
      }
    }
  }

  /**
   * Поиск по нодам.
   */
  private function getNodes() {
    $result = $this->entitiesBaseQuery('node');
    $nodesTotal = $result->getResultCount();
    $this->response['total'] += $nodesTotal;

    $watchedNow = self::ITEMS_LIMIT * ($this->page + 1);
    if ($watchedNow > $nodesTotal) {
      $watchedNow = $nodesTotal;
    }
    $this->response['watched'] += $watchedNow;

    $nodes = [];
    $count = 0;
    foreach ($result->getResultItems() as $item) {
      if ($count >= ($this->page * self::ITEMS_LIMIT) && count($nodes) < self::ITEMS_LIMIT) {
        $nodes[] = $item->getOriginalObject()->getEntity();
      }
      $count++;
    }
    return $nodes;
  }

  /**
   * Base structure of entity query request.
   */
  private function entitiesBaseQuery(string $table) {
    $index = $this->index_storage->load($table);
    $query = $index->query()
      ->keys($this->title)
      ->sort('title', 'ASC')
      ->execute();
    return $query;
  }

  /**
   * Associative array sort with keys.
   */
  private function associativeArraySortWithKey(&$array, $insideKey, $reverse = FALSE) {
    $simpleArray = [];
    foreach ($array as $key => $value) {
      $simpleArray[] = [
        'key' => $key,
        'value' => $value,
        'inside' => $value[$insideKey],
      ];
    }
    self::bubleSort($simpleArray, 'inside', $reverse);
    $array = [];
    foreach ($simpleArray as $element) {
      $array[$element['key']] = $element['value'];
    }
  }

  /**
   * Buble sort.
   */
  private function bubleSort(&$array, $key, $reverse = FALSE) {
    if (count($array) && isset($array[0][$key])) {
      for ($j = 1; $j < count($array); $j++) {
        for ($i = 0; $i < count($array) - $j; $i++) {
          if ($array[$i][$key] > $array[$i + 1][$key]) {
            self::swapElement($array, $i, $i + 1);
          }
        }
      }
    }
    if ($reverse) {
      $array = array_reverse($array);
    }
  }

  /**
   * Swap array element.
   */
  private function swapElement(&$array, $i, $j) {
    $buf = $array[$i];
    $array[$i] = $array[$j];
    $array[$j] = $buf;
  }

  /**
   * Get all allowed for search node types.
   */
  private function getAllowedNodeTypes() {
    $values = $this->config->get('node_types', 'page');
    $types = [];
    foreach (explode(',', $values) as $type) {
      $types[] = trim($type);
    }
    return $types;
  }

  /**
   * Make base empty array for response.
   */
  private function makeBaseResponseArray() {
    $this->response = [
      'limit' => self::ITEMS_LIMIT,
      'title' => $this->title,
      'watched' => 0,
      'total' => 0,
      'list' => [],
    ];
  }

}
