/**
 * @file
 */

(function () {
  "use strict";

  Vue.component('synsearch', {
    data() {
      return {
        page: 0,
        limit: 0,
        watched: 0,
        total: 0,
        items: [],
        title: '',
        noResult: false,
      }
    },
    props: {
      data: {},
    },
    methods: {
      getReviews(method = 'no-merge') {
        axios.get('/syn-search/api/get-list?title='+ this.title +'', {
          params: {
            page: this.page,
          },
        }).then((response) => {
          if (method == 'merge') {
            this.items = this.items.concat(response.data.list);
          }
          else {
            this.items = response.data.list;
          }
          this.total = response.data.total;
          if (!this.total) {
            this.noResult = true;
          }
          this.limit = response.data.limit;
          this.watched = response.data.watched;
        });
      },
      loadMore() {
        console.log('2');
        this.page++;
        this.getReviews('merge');
      },
    },
    created() {
      this.title = this.data.title;
      if (this.title) {
        this.getReviews();
      }
    },
  });

})();
